Jika mengerjakan Task Jangan Lupa Untuk menambah Label pada Task

Contoh : 

1. Jenis Pengguna
    1. (FITUR) lpse2 hanya admin ppe, sisanya lpse1 (sedang dikerjakan DENY)

Jika telah selesai mengerjakan jangan lupa untuk memberi tanda juga

Contoh : 

1. Jenis Pengguna
    1. (FITUR) lpse2 hanya admin ppe, sisanya lpse1 (Selesai oleh DENY)


=============================================================================================================================

@Syahrul Ramadhan @Indra Setiawan @Deny Prasetyo 

eproc 3.0 - testing

Testing Berikutnya Jum'at, 23 Agustus 2019

1. Pekerjaan yang harus diselesaikan. 
2. Hapus tiket dan relasinya untuk testing berikutnya


TBD:
1. Indexing


1. Jenis Pengguna
    1. (FITUR) lpse2 hanya admin ppe, sisanya lpse1
2. Daftar Tiket (PELAPOR)
    1. (FITUR+DESIGN) Pencarian pada daftar tiket
        1. Status tiket
        2. Nomor tiket
    2. (FITUR+DESIGN) 
        1. Sorting tiket berdasarkan tiket terbaru.  
        2. Tetapi kalo ada aksi yang harus segera dilakukan, seperti feedback dan pemberian rating taruh di atas.
        3. Jika sudah selesai, dibuat paling belakang saja
    3. Keterangan pada daftar tiket
        1. (DISKUSI) 
            1. Menunggu dengan menunggu feedback bedanya apa?
                1. (FITUR+DESIGN) Jika, menunggu itu menunggu dikerjakan, tulis saja menunggu dikerjakan
        2. (FITUR+DESIGN)
            1. label dikerjakan -> tambahkan sedang dikerjakan
        3. (DESIGN) 
            1. Icon dan warna pemberian rating dibedakan dengan menunggu
            2. Icon dan warna dikerjakan dibedakan dengan menunggu
    4. (DESIGN) Peletakan tulisan daftar tiket sejajar dengan button buat Tiket
3. Buat Tiket (PELAPOR)
    1. Penyedia
        1. (FITUR+DESIGN) Tidak bisa ubah NPWP. NPWP akan terus diupdate yang dapet dari adp. 
            1. (DISKUSI)  kendalanya dulu apa kenapa npwp dan email disuruh isi ulang? Gak dapet email ya dari spse? Jika email gak dapet dari SPSE, label email saja yang bisa diubah
                1. Jika email disimpan, hanya relasi dengan tiket tersebut
                2. Jika email, xxx@user.com, anggep belum memiliki email dengan label "Email belum ada"
                3. Tambahkan icon tanda tanya untuk informasi, bunyinya "Lengkapi email pada menu profil pengguna"
    2. Non Penyedia
        1. (FITUR+DESIGN) Bisa melakukan ubah Nama, NIP, dan email.
            1. Nama dan NIP tidak bisa diubah
                1. Jika email disimpan, hanya relasi dengan tiket tersebut
                2. Jika email, xxx@user.com, anggep belum memiliki email dengan label "Email belum ada"
                3. Tambahkan icon tanda tanya untuk informasi, bunyinya "Lengkapi email pada menu profil pengguna"
    3. Buat tiket secara umum
        1. (DISKUSI) Berapa banyak karakter untuk deskripsi?
            1. (FITUR) tidak terbatas (Text? Varchar?)
        2. (FITUR+DESIGN) Tambahkan kode tender, ada pilihan untuk mencari terlebih dahulu, jika tidak bisa dicari, isikan manual (Seperti di INAPROC Daftar Hitam)
        3. (FITUR+DESIGN) Aplikasi yang hanya ada 1 versi, dropdownnya di-disable. versi aplikasi dibuat pakai default
        4. (FITUR) Kode error tidak perlu mandatory
        5. (FITUR) Tulisan pada deskripsi
            1. minimal 15 karakter dibuat lebih jelas dan informatif 
            2. Kata-kata jangan dibuat lebih jelas dan informatif (Merah warnanya)
        6. (FITUR+DESIGN) Jika sudah upload file, tidak ada tombol untuk hapus file. sama kaya lpse buat tiket
        7. (FITUR+DESIGN)  Pop up untuk konfirmasi dibuat seragam dengan pop up lainnya. Diperbaiki. 
            1. Judul tiket diletakkan di bawahnya nama lpse.
            2. Tambahkan nama aplikasi dan versinya di bawah judul tiket
            3. Profil yang dimasukkan
                1. NPWP/NIP hilangkan dan nama perusahaan/nama orang
                2. Email tetap ada
        8. (BUG FIXING) 
            1. Upload file gagal -- (Selesai oleh DENY)
            2. Validasi mandatory, format file upload-an, dan jumlah karakter pada field judul dan deskripsi -- (Selesai oleh DENY validasi LPSE1 & LPSE2)
4. Daftar Tiket - TIKET MASUK (LPSE LEVEL 1)
    1. (DESIGN) Informasi tiket (Mas Deny)
        1. Peletakan aplikasi dituker dengan asal lpse. Versinya ditaruh disebelah nama aplikasi. -- (Selesai oleh DENY)
        2. Kategori dan prioritas dihilangkan karena belum ada -- (Selesai oleh DENY)
        3. Lama pengerjaan digeser ke kiri, di tengah, button ambil tiket di tengah juga berarti -- (Selesai oleh DENY)
    2. (FITUR+DESIGN) Filter 
        1. default pencarian nomor tiket -- (Selesai oleh DENY)
        2. Sisanya advanced search, collapse -- (Selesai oleh DENY)
            1. Pilih aplikasi dan versi jangan atas bawah, sebelahan saja. -- (Selesai oleh DENY)
            2. Durasi tanggal yang agak panjang datepickernya. Tambahkan label "Tanggal dibuat" -- (Selesai oleh DENY)
            3. Judul Tiket -- (Selesai oleh DENY)
    3. (FITUR) Sorting
        1. Berdasarkan tiket yang dieskalasi terlama
5. Ambil Tiket (LPSE Level 1)
    1. (FITUR+DESIGN) Hilangkan/hide semua Tipe Tiket pada semua halaman yang ada halaman tipe Tiket
    2. (FITUR+DESIGN) Tiket sebelum diambil, tidak dapat diubah data-datanya (Saat ini dapat diubah)
    3. (FITUR + DESIGN) Kode error ada dua field.
        1. Kode error dan kode tender tidak boleh diubah oleh siapapun (LPSE dan LKPP)
        2. Tapi pelapor bisa ubah kode error atau kode tender ketika diminta feedback
    4. (FITUR+DESIGN) Aplikasi yang hanya ada 1 versi, dropdownnya di-disable. versi aplikasi dibuat pakai default
    5. (BUG FIXING) Kategori aplikasi tidak bisa dipilih. Ngeblink
    6. (FITUR) tambahkan label upload file, jika tidak ada tulis "Tidak ada file yang di-upload"
6. Review Tiket (LPSE Level 1)
    1. (FITUR) Limpahkan ganti jadi eskalasi
    2. (FITUR) Ketika pilih tindakan, Tulisannya eskalasi tiket saja. Eskalasikan tiket misalnya ke Helpdesk monev, tulisan "Helpdesk Monev" letakkan di bawahnya saja (Seperti yang "Butuh Feedback")
    3. (FITUR+DESIGN) Tidak perlu ada label khusus untuk versi aplikasi, gabungkan saja dengan aplikasi
    4. (FITUR+DESIGN)  LPSE tidak bisa mengubah aplikasi. Versi aplikasi bisa diubah hanya SPSE
    5. (FITUR+DESIGN) Font label
        1. Font aplikasi diperjelas
        2. Prioritas diletakkan di bawah aktegori tiket, usahakan terlihat jelas
    6. (FITUR+DESIGN)  Deskripsi tiket dibuat seperti diskusi judul labelnya, tambahkan garis untuk membedakan dengan informasi tiket lainnya
7. Daftar Tiket Ditangani
    1. (FITUR+DESIGN) Filter 
        1. default pencarian nomor tiket
        2. Sisanya advanced search, collapse
            1. Pilih aplikasi dan versi jangan atas bawah, sebelahan saja.
            2. Durasi tanggal yang agak panjang datepickernya. Tambahkan label "Tanggal dibuat"
            3. Judul Tiket
            4. Status Tiket
    2. (FITUR) Sorting berdasarkan tiket yang diambil terlama. Tetapi kalo ada aksi yang harus segera dilakukan, seperti feedback dan pemberian rating taruh di atas
    3. (FITUR+DESIGN) Menunggu review diganti jadi menunggu dikerjakan
8. Semua review ganti jadi kerjakan
9. Buat Tiket (LPSE Level 1)
10. Buat Tiket (LPSE Level 2)
    1. (FITUR) Inputannya
        1. IP server di masking semuanya
        2. Password bisa dilait jika diklik
    2. 
11. Nomor Tiket (FITUR) 
    1. PMEP
        1. Pelapor: TP | M | YYMMDD | 0^4
        2. LPSE Lvl 1: TH | M | YYMMDD | 0^4
        3. LPSE Lvl 2: TA | M | YYMMDD | 0^4
        4. Konsul: TK | M | YYMMDD | 0^4
    2. KATALOG
        1. Pelapor: TP | K | YYMMDD | 0^4
        2. LPSE Lvl 1: TH | K | YYMMDD | 0^4
        3. LPSE Lvl 2: TA | K | YYMMDD | 0^4
        4. Konsul: TK | K | YYMMDD | 0^4
    3. SPSE
        1. Pelapor: TP | S | YYMMDD | 0^4 -> TPSP1901010001
        2. LPSE Lvl 1: TH | S | YYMMDD | 0^4
        3. LPSE Lvl 2: TA | S | YYMMDD | 0^4
        4. Konsul: TK | S | YYMMDD | 0^4
12. Prioritas (FITUR)
    1. Semua prioritas tidak ada inputan dari user, kecuali perubahan paksa oleh Supervisor dan Eselon 4,3,2,1
    2. Prioritas menggunakan formula, setiap kategori tiket memiliki minimal 2 value, untuk diinputkan suatu angka
    3. Terdapat 4 kategori prioritas: Emergency, Major, Medium, Minor
    4. Masukkan semua indikator penentuan prioritas dan kategori permasalahan dari excel yang dikasih
13. Jenis Aplikasi
    1. Jenis aplikasi sudah disesuaikan dengan production
14. Eselon 4 tus dapat mengerjakan tiket
15. Eselon 4 tus/itd/ito dan supervisor dapat reassign tiket